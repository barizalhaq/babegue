package com.example.babegue.data.model;

public class PanduanAnak {

    private String title;
    private String desc;
    private int cardStyle;
    private int headerImg;

    public PanduanAnak(String title, String desc, int cardStyle, int headerImg) {
        this.title = title;
        this.desc = desc;
        this.cardStyle = cardStyle;
        this.headerImg = headerImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getCardStyle() {
        return cardStyle;
    }

    public void setCardStyle(int cardStyle) {
        this.cardStyle = cardStyle;
    }

    public int getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(int headerImg) {
        this.headerImg = headerImg;
    }
}
