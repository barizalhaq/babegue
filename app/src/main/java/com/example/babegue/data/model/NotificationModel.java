package com.example.babegue.data.model;

import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Date;

public class NotificationModel {

    private String title;
    private String message;
    private String namaAnak;
    private Long timestamp;
    private Boolean active;

    public NotificationModel() {
    }

    public NotificationModel(String title, String message, String namaAnak,
                             Long timestamp, @Nullable Boolean active) {
        this.title = title;
        this.message = message;
        this.namaAnak = namaAnak;
        this.timestamp = timestamp;
        if (active == null) {
            this.active = true;
        } else {
            this.active = false;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNamaAnak() {
        return namaAnak;
    }

    public void setNamaAnak(String namaAnak) {
        this.namaAnak = namaAnak;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
