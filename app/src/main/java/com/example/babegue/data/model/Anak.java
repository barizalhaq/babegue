package com.example.babegue.data.model;

import java.util.HashMap;
import java.util.Map;

public class Anak {

    private String namaAnak;
    private String tlAnak;
    private String jKelamin;
    private String idAnak;

    public Anak() {
    }

    public Anak(String namaAnak, String tlAnak, String jKelamin) {
        this.namaAnak = namaAnak;
        this.tlAnak = tlAnak;
        this.jKelamin = jKelamin;
    }

    public Anak(String namaAnak, String tlAnak, String jKelamin, String idAnak) {
        this.namaAnak = namaAnak;
        this.tlAnak = tlAnak;
        this.jKelamin = jKelamin;
        this.idAnak = idAnak;
    }

    public String getNamaAnak() {
        return namaAnak;
    }

    public void setNamaAnak(String namaAnak) {
        this.namaAnak = namaAnak;
    }

    public String getTlAnak() {
        return tlAnak;
    }

    public void setTlAnak(String tlAnak) {
        this.tlAnak = tlAnak;
    }

    public String getjKelamin() {
        return jKelamin;
    }

    public void setjKelamin(String jKelamin) {
        this.jKelamin = jKelamin;
    }

    public String getIdAnak() {
        return idAnak;
    }

    public void setIdAnak(String idAnak) {
        this.idAnak = idAnak;
    }
}
