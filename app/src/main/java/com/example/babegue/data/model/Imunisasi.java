package com.example.babegue.data.model;

import android.util.ArrayMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Imunisasi {

    private String titleImunisasi;
    private String descImunisasi;
    private String[] nameImunisasi;
    private String idAnak;

    public Imunisasi() {
    }

    public Imunisasi(String titleImunisasi, String descImunisasi, String[] nameImunisasi) {
        this.titleImunisasi = titleImunisasi;
        this.descImunisasi = descImunisasi;
        this.nameImunisasi = nameImunisasi;
    }

    public Imunisasi(String titleImunisasi, String descImunisasi, String[] nameImunisasi,
                     String idAnak) {
        this.titleImunisasi = titleImunisasi;
        this.descImunisasi = descImunisasi;
        this.nameImunisasi = nameImunisasi;
        this.idAnak = idAnak;
    }

    public String getTitleImunisasi() {
        return titleImunisasi;
    }

    public String getDescImunisasi() {
        return descImunisasi;
    }

    public String[] getNameImunisasi() {
        return nameImunisasi;
    }

    public void setTitleImunisasi(String titleImunisasi) {
        this.titleImunisasi = titleImunisasi;
    }

    public void setDescImunisasi(String descImunisasi) {
        this.descImunisasi = descImunisasi;
    }

    public void setNameImunisasi(String[] nameImunisasi) {
        this.nameImunisasi = nameImunisasi;
    }

}
