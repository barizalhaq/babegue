package com.example.babegue.data.model;

import androidx.annotation.Nullable;

public class GiziNutrisi {

    private String titleAgeReq;
    private String captionTitle;
    private int cardStyle;
    private int headerImg;
    private int attachedImg;

    public int getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(int headerImg) {
        this.headerImg = headerImg;
    }

    public int getAttachedImg() {
        return attachedImg;
    }

    public void setAttachedImg(int attachedImg) {
        this.attachedImg = attachedImg;
    }

    public GiziNutrisi(String titleAgeReq, String captionTitle, @Nullable int cardStyle, int headerImg, int attachedImg) {
        this.titleAgeReq = titleAgeReq;
        this.captionTitle = captionTitle;
        this.cardStyle = cardStyle;
        this.headerImg = headerImg;
        this.attachedImg = attachedImg;
    }

    public String getTitleAgeReq() {
        return titleAgeReq;
    }

    public String getCaptionTitle() {
        return captionTitle;
    }

    public int getCardStyle() {
        return cardStyle;
    }

    public void setTitleAgeReq(String titleAgeReq) {
        this.titleAgeReq = titleAgeReq;
    }

    public void setCaptionTitle(String captionTitle) {
        this.captionTitle = captionTitle;
    }

    public void setCardStyle(int cardStyle) {
        this.cardStyle = cardStyle;
    }
}
