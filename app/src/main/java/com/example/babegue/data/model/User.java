package com.example.babegue.data.model;

import java.util.Map;

public class User {

    private String fullName;
    private String email;
    private Map<String, Boolean> babies;

    public User() {
    }

    public User(String fullName, String email) {
        this.fullName = fullName;
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Boolean> getBabies() {
        return babies;
    }

    public void setBabies(Map<String, Boolean> babies) {
        this.babies = babies;
    }
}
