package com.example.babegue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private Button registerButton;
    private FirebaseAuth mAuth;
    private EditText userEmail;
    private EditText userPassword;
    private FloatingActionButton loginButton;
    private ProgressBar progressBar;
    AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.input_email, Patterns.EMAIL_ADDRESS,
                R.string.email_err);
        awesomeValidation.addValidation(this, R.id.input_password, "^(?=\\s*\\S).*$",
                R.string.pass_err);

        registerButton = (Button) findViewById(R.id.reg_button);
        mAuth = FirebaseAuth.getInstance();

        userEmail = (EditText) findViewById(R.id.input_email);
        userPassword = (EditText) findViewById(R.id.input_password);
        loginButton = (FloatingActionButton) findViewById(R.id.login_button);

        loginButton.setOnClickListener(v -> {

            if (awesomeValidation.validate()) {
                String email = userEmail.getText().toString();
                String password = userPassword.getText().toString();

                if (! email.equals("") && ! password.equals("")) {
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(LoginActivity.this, task -> {
                                if (task.isSuccessful()) {
                                    FirebaseUser user = mAuth.getCurrentUser();

                                    progressBar = (ProgressBar) findViewById(R.id.progress_loader);
                                    progressBar.setVisibility(View.VISIBLE);
                                    progressBar.setIndeterminate(true);

                                    startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }

        });

        registerButton.setOnClickListener(v -> {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            finish();
        });
    }
}
