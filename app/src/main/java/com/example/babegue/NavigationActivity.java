package com.example.babegue;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.LayoutInflater;

import com.example.babegue.helper.ShowAddAnakDialog;
import com.example.babegue.helper.ShowNotification;
import com.example.babegue.helper.TimeToImunisasi;
import com.example.babegue.ui.account.AccountFragment;
import com.example.babegue.ui.dashboard.DashboardFragment;
import com.example.babegue.ui.home.HomeFragment;
import com.example.babegue.ui.notifications.NotificationsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.sql.Time;

import static com.example.babegue.Baymax.CHANNEL_1_ID;
import static com.example.babegue.Baymax.CHANNEL_2_ID;

public class NavigationActivity extends AppCompatActivity {

    private NotificationManagerCompat notificationManager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = (item) -> {
        switch (item.getItemId()) {
            case R.id.navigation_dashboard:
                DashboardFragment dashboardFragment = new DashboardFragment();
                FragmentTransaction dashboardTransaction = getSupportFragmentManager().beginTransaction();
                dashboardTransaction.replace(R.id.fragment_content, dashboardFragment);
                dashboardTransaction.commit();
                return true;
            case R.id.navigation_account:
                AccountFragment accountFragment = new AccountFragment();
                FragmentTransaction accountTransaction = getSupportFragmentManager().beginTransaction();
                accountTransaction.replace(R.id.fragment_content, accountFragment);
                accountTransaction.commit();
                return true;
            case R.id.navigation_notifications:
                NotificationsFragment notificationsFragment = new NotificationsFragment();
                FragmentTransaction notificationsTransaction = getSupportFragmentManager().beginTransaction();
                notificationsTransaction.replace(R.id.fragment_content, notificationsFragment);
                notificationsTransaction.commit();
                return true;
        }
        return false;
    };

    private DatabaseReference databaseReference;
    private LayoutInflater layoutInflater;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;

    private ValueEventListener valueEventListener;
    private String receivedIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        setContentView(R.layout.activity_navigation);

        layoutInflater = getLayoutInflater();
        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        showNotification(this);
        receivedIntent = getIntent().getStringExtra("NotificationModel");

        if (receivedIntent != null) {
            NotificationsFragment notificationsFragment = new NotificationsFragment();
            FragmentTransaction notificationsTransaction = getSupportFragmentManager().beginTransaction();
            notificationsTransaction.replace(R.id.fragment_content, notificationsFragment);
            notificationsTransaction.commit();
        } else {
            DashboardFragment dashboardFragment = new DashboardFragment();
            FragmentTransaction dashboardTransaction = getSupportFragmentManager().beginTransaction();
            dashboardTransaction.replace(R.id.fragment_content, dashboardFragment);
            dashboardTransaction.commit();
        }

        BottomNavigationView navView = (BottomNavigationView) findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        showNotification(this.getApplicationContext());

        databaseReference.child("DataParents").child("Parents")
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(firebaseUser.getUid())) {
                    ShowAddAnakDialog.showDialog(R.layout.form_anak, layoutInflater, true, NavigationActivity.this);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showNotification(Context context) {
        new TimeToImunisasi(context).checkTimeToImunisasi();
    }
}
