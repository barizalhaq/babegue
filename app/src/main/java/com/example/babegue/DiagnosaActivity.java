package com.example.babegue;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.babegue.helper.DiagnosaAnak;

import java.lang.reflect.Array;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DiagnosaActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewGroup viewGroup;
    private CardView cardBatuk;
    private CardView cardDiare;
    private CardView cardDemam;
    private TextView batukLabelChild;
    private TextView batukLabelSesak;
    private TextView diareLabelDarah;
    private TextView diareLabelDemam;
    private TextView diareLabelMuntah;
    private TextView diareLabelKonsumsi;
    private TextView demamLabelDiare;
    private TextView demamLabelBatuk;
    private TextView demamLabelBintik;
    private TextView demamLabelHidung;
    private TextView demamLabelFeses;
    private TextView demamLabelKejang;
    private TextView demamLabelEndemis;
    private RadioGroup batukRadioChild;
    private RadioGroup batukRadioSesak;
    private RadioGroup diareForm;
    private RadioGroup diareFormDemam;
    private RadioGroup diareFormDarah;
    private RadioGroup diareFormMuntah;
    private RadioGroup diareFormKonsumsi;
    private RadioGroup demamFormDiare;
    private RadioGroup demamFormBatuk;
    private RadioGroup demamFormBintik;
    private RadioGroup demamFormHidung;
    private RadioGroup demamFormFeses;
    private RadioGroup demamFormKejang;
    private RadioGroup demamFormEndemis;
    private RadioGroup lamaSakit;
    private RadioGroup usiaAnak;

    private Button diagnosaButton;
    private Map<String, Boolean> hasil;

    private int radioCheck = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosa);

        initialize();

        RadioButton batukNG = findViewById(R.id.radio_batuk_ng);
        batukNG.setOnClickListener(this);
        RadioButton batukPS = findViewById(R.id.radio_batuk_ps);
        batukPS.setOnClickListener(this);
        RadioButton diareNG = findViewById(R.id.radio_ng_diare);
        diareNG.setOnClickListener(this);
        RadioButton diarePS = findViewById(R.id.radio_ps_diare);
        diarePS.setOnClickListener(this);
        RadioButton demamNG = findViewById(R.id.radio_ng_demam);
        demamNG.setOnClickListener(this);
        RadioButton demamPS = findViewById(R.id.radio_ps_demam);
        demamPS.setOnClickListener(this);

        diagnosaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioCheck < 0) {
                    new AlertDialog.Builder(DiagnosaActivity.this).setTitle("Peringatan")
                            .setMessage("Format yang anda masukkan salah. " +
                                    "Tidak boleh ada yang kosong")
                            .setNegativeButton("OK", null).show();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {

        boolean visible = false;

        switch (view.getId()) {
            case R.id.radio_batuk_ps:
                setBatukFormat(!visible);
                break;
            case R.id.radio_batuk_ng:
                setBatukFormat(visible);
                break;
            case R.id.radio_ps_diare:
                setDiareFormat(!visible);
                break;
            case R.id.radio_ng_diare:
                setDiareFormat(visible);
                break;
            case R.id.radio_ps_demam:
                setDemamFormat(!visible);
                break;
            case R.id.radio_ng_demam:
                setDemamFormat(visible);
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void setBatukFormat(boolean visible) {
//        for (int i = 0; i < diareForm.getChildCount(); i++)
//            ((RadioButton) diareForm.getChildAt(i)).setEnabled(false);
        TransitionManager.beginDelayedTransition(viewGroup);
        cardDiare.setVisibility(!visible ? View.VISIBLE : View.GONE);
        cardDemam.setVisibility(!visible ? View.VISIBLE : View.GONE);
        batukLabelChild.setVisibility(visible ? View.VISIBLE : View.GONE);
        batukRadioChild.setVisibility(visible ? View.VISIBLE : View.GONE);
        batukLabelSesak.setVisibility(visible ? View.VISIBLE : View.GONE);
        batukRadioSesak.setVisibility(visible ? View.VISIBLE : View.GONE);

        setCheckedListener(batukRadioChild, "G03", R.id.batuk_demam_ps);
        setCheckedListener(batukRadioSesak, "G04", R.id.batuk_sesak_ps);
        setCheckedListener(lamaSakit, "G13", R.id.periode_hari_lebih);
        setCheckedListener(usiaAnak, "G14", R.id.usia_anak_lebih);
        diagnosaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    DiagnosaAnak.runDiagnosaBatuk(hasil, DiagnosaActivity.this);
                    if (radioCheck < 0) {
                        throw new Exception();
                    }
                } catch (Exception e) {
                    new AlertDialog.Builder(DiagnosaActivity.this).setTitle("Peringatan")
                            .setMessage("Format yang anda masukkan salah. " +
                                    "Tidak boleh ada yang kosong")
                            .setNegativeButton("OK", null).show();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void setDiareFormat(boolean visible) {
        TransitionManager.beginDelayedTransition(viewGroup);
        cardBatuk.setVisibility(!visible ? View.VISIBLE : View.GONE);
        cardDemam.setVisibility(!visible ? View.VISIBLE : View.GONE);
        diareFormDemam.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareFormDarah.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareFormMuntah.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareFormKonsumsi.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareLabelDemam.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareLabelDarah.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareLabelMuntah.setVisibility(visible ? View.VISIBLE : View.GONE);
        diareLabelKonsumsi.setVisibility(visible ? View.VISIBLE : View.GONE);

        setCheckedListener(diareFormDemam, "G03", R.id.diare_demam_ps);
        setCheckedListener(diareFormDarah, "G05", R.id.diare_feses_ps);
        setCheckedListener(diareFormMuntah, "G06", R.id.diare_muntah_ps);
        setCheckedListener(diareFormKonsumsi, "G07", R.id.diare_konsumsi_ps);
        setCheckedListener(lamaSakit, "G13", R.id.periode_hari_lebih);
        setCheckedListener(usiaAnak, "G14", R.id.usia_anak_lebih);
        diagnosaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    DiagnosaAnak.runDiagnosaDiare(hasil, DiagnosaActivity.this);
                } catch (Exception e) {
                    new AlertDialog.Builder(DiagnosaActivity.this).setTitle("Peringatan")
                            .setMessage("Format yang anda masukkan salah. " +
                                    "Tidak boleh ada yang kosong")
                            .setNegativeButton("OK", null).show();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void setDemamFormat(boolean visible) {
        TransitionManager.beginDelayedTransition(viewGroup);
        cardBatuk.setVisibility(!visible ? View.VISIBLE : View.GONE);
        cardDiare.setVisibility(!visible ? View.VISIBLE : View.GONE);
        demamFormBatuk.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamFormDiare.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamFormBintik.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamFormHidung.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamFormFeses.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamFormKejang.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamFormEndemis.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelDiare.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelBatuk.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelBintik.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelHidung.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelFeses.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelKejang.setVisibility(visible ? View.VISIBLE : View.GONE);
        demamLabelEndemis.setVisibility(visible ? View.VISIBLE : View.GONE);

        setCheckedListener(demamFormDiare, "G02", R.id.demam_diare_ps);
        setCheckedListener(demamFormBintik, "G09", R.id.demam_bintik_ps);
        setCheckedListener(demamFormHidung, "G10", R.id.demam_hidung_ps);
        setCheckedListener(demamFormFeses, "G11", R.id.demam_feses_ps);
        setCheckedListener(demamFormKejang, "G08", R.id.demam_kejang_ps);
        setCheckedListener(demamFormBatuk, "G01", R.id.batuk_demam_ps);
        setCheckedListener(demamFormEndemis, "G12", R.id.demam_endemis_ps);
        setCheckedListener(lamaSakit, "G13", R.id.periode_hari_lebih);
        setCheckedListener(usiaAnak, "G14", R.id.usia_anak_lebih);

        diagnosaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    DiagnosaAnak.runDiagnosaDemam(hasil, DiagnosaActivity.this);
                    if (radioCheck < 0) {
                        throw new Exception();
                    }
                } catch (Exception e) {
                    new AlertDialog.Builder(DiagnosaActivity.this).setTitle("Peringatan")
                            .setMessage("Format yang anda masukkan salah. " +
                                    "Tidak boleh ada yang kosong")
                            .setNegativeButton("OK", null).show();
                }
            }
        });
    }

    public void initialize() {
        hasil = new HashMap<String, Boolean>();
        viewGroup = findViewById(R.id.form_diagnosa);

//        Card
        cardBatuk = (CardView) findViewById(R.id.batuk_card);
        cardDiare = (CardView) findViewById(R.id.diare_card);
        cardDemam = (CardView) findViewById(R.id.demam_card);

//        Batuk
        batukLabelChild = (TextView) findViewById(R.id.batuk_label_demam);
        batukRadioChild = (RadioGroup) findViewById(R.id.form_batuk_demam);
        batukLabelSesak = (TextView) findViewById(R.id.batuk_label_sesak);
        batukRadioSesak = (RadioGroup) findViewById(R.id.form_batuk_sesak);

//        Diare
        diareForm           = (RadioGroup) findViewById(R.id.form_diare);
        diareFormDemam      = (RadioGroup) findViewById(R.id.form_diare_demam);
        diareFormDarah      = (RadioGroup) findViewById(R.id.form_diare_darah);
        diareFormMuntah     = (RadioGroup) findViewById(R.id.form_diare_muntah);
        diareFormKonsumsi   = (RadioGroup) findViewById(R.id.form_diare_konsumsi);
        diareLabelDemam     = (TextView) findViewById(R.id.diare_label_demam);
        diareLabelDarah     = (TextView) findViewById(R.id.diare_label_darah);
        diareLabelMuntah    = (TextView) findViewById(R.id.diare_label_muntah);
        diareLabelKonsumsi  = (TextView) findViewById(R.id.diare_label_konsumsi);

//        Demam
        demamFormBatuk      = (RadioGroup) findViewById(R.id.form_demam_batuk);
        demamFormDiare      = (RadioGroup) findViewById(R.id.form_demam_diare);
        demamFormBintik     = (RadioGroup) findViewById(R.id.form_demam_bintik);
        demamFormHidung     = (RadioGroup) findViewById(R.id.form_demam_hidung);
        demamFormFeses      = (RadioGroup) findViewById(R.id.form_demam_tinja);
        demamFormKejang     = (RadioGroup) findViewById(R.id.form_demam_kejang);
        demamFormEndemis     = (RadioGroup) findViewById(R.id.form_demam_endemis);
        demamLabelDiare     = (TextView) findViewById(R.id.demam_label_diare);
        demamLabelBatuk     = (TextView) findViewById(R.id.demam_label_batuk);
        demamLabelBintik    = (TextView) findViewById(R.id.demam_label_bintik);
        demamLabelHidung    = (TextView) findViewById(R.id.demam_label_hidung);
        demamLabelFeses     = (TextView) findViewById(R.id.demam_label_tinja);
        demamLabelKejang    = (TextView) findViewById(R.id.demam_label_kejang);
        demamLabelEndemis    = (TextView) findViewById(R.id.demam_label_endemis);

        lamaSakit = (RadioGroup) findViewById(R.id.form_periode_hari);
        usiaAnak = (RadioGroup) findViewById(R.id.form_usia_anak);

        diagnosaButton = (Button) findViewById(R.id.diagnosa_button);

        setVisibleGone();
    }

    public void setVisibleGone() {
        batukLabelChild.setVisibility(View.GONE);
        batukRadioChild.setVisibility(View.GONE);
        batukLabelSesak.setVisibility(View.GONE);
        batukRadioSesak.setVisibility(View.GONE);
        diareFormDemam.setVisibility(View.GONE);
        diareFormDarah.setVisibility(View.GONE);
        diareFormMuntah.setVisibility(View.GONE);
        diareFormKonsumsi.setVisibility(View.GONE);
        diareLabelDemam.setVisibility(View.GONE);
        diareLabelDarah.setVisibility(View.GONE);
        diareLabelMuntah.setVisibility(View.GONE);
        diareLabelKonsumsi.setVisibility(View.GONE);
        demamFormBatuk.setVisibility(View.GONE);
        demamFormDiare.setVisibility(View.GONE);
        demamFormBintik.setVisibility(View.GONE);
        demamFormHidung.setVisibility(View.GONE);
        demamFormFeses.setVisibility(View.GONE);
        demamFormKejang.setVisibility(View.GONE);
        demamLabelDiare.setVisibility(View.GONE);
        demamLabelBatuk.setVisibility(View.GONE);
        demamLabelBintik.setVisibility(View.GONE);
        demamLabelHidung.setVisibility(View.GONE);
        demamLabelFeses.setVisibility(View.GONE);
        demamLabelKejang.setVisibility(View.GONE);
        demamLabelEndemis.setVisibility(View.GONE);
        demamFormEndemis.setVisibility(View.GONE);
    }

    private void setCheckedListener(RadioGroup radioGroup, String key, int _checkedId) {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == _checkedId) {
                    hasil.put(key, true);
                    radioCheck = radioGroup.getCheckedRadioButtonId();
                } else {
                    hasil.put(key, false);
                    radioCheck = radioGroup.getCheckedRadioButtonId();
                }
            }
        });
    }
}