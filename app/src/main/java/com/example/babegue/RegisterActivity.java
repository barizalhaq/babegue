package com.example.babegue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.babegue.R;
import com.example.babegue.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class RegisterActivity extends AppCompatActivity {

    private Button loginButton;
    private EditText fullName;
    private EditText emailUser;
    private EditText passwordUser;
    private FloatingActionButton registerButton;
    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;
    private String userId;
    private AwesomeValidation validation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initialize();

        registerButton.setOnClickListener(v -> {

            if (validation.validate()) {
                String fullname = fullName.getText().toString();
                String email = emailUser.getText().toString();
                String password = passwordUser.getText().toString();

                if(!email.equals("") && !password.equals("")) {
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(RegisterActivity.this, task -> {
                                if (task.isSuccessful()) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    assert user != null;
                                    addUser(fullname, email, user.getUid());
                                    Toast.makeText(RegisterActivity.this, "Pendaftaran Berhasil!",
                                            Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Pendaftaran Gagal!",
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                }
            }
        });

        loginButton.setOnClickListener(v -> {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            finish();
        });
    }

    private void initialize() {
        loginButton         = (Button) findViewById(R.id.login_button);
        mAuth               = FirebaseAuth.getInstance();
        registerButton      = (FloatingActionButton) findViewById(R.id.register);
        fullName            = (EditText) findViewById(R.id.input_fullname);
        emailUser           = (EditText) findViewById(R.id.input_email);
        passwordUser        = (EditText) findViewById(R.id.input_password);
        firebaseDatabase    = FirebaseDatabase.getInstance();
        databaseReference   = firebaseDatabase.getReference("DataUsers");

        validation = new AwesomeValidation(BASIC);
        validation.addValidation(this, R.id.input_fullname, "^(?=\\s*\\S).*$",
                R.string.fullname_err);
        validation.addValidation(this, R.id.input_email, Patterns.EMAIL_ADDRESS,
                R.string.email_err);
        validation.addValidation(this, R.id.input_password, "^(?=\\s*\\S).*$",
                R.string.pass_err);
        validation.addValidation(this, R.id.input_password, "^[A-Za-z0-9\\s]{6,}$",
                R.string.pass_minimum);
    }

    private void addUser(String fullName, String email, String userId) {
        this.userId = userId;
        User user = new User(fullName, email);
        databaseReference.child("Users").child(this.userId).setValue(user);
    }
}
