package com.example.babegue.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.babegue.DetailedGiziNutrisi;
import com.example.babegue.R;
import com.example.babegue.data.model.GiziNutrisi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class GiziNutrisiAdapter extends RecyclerView.Adapter<GiziNutrisiAdapter.GiziNutrisiHolder> {

    private Context mContext;
    private List<GiziNutrisi> mData;

    public GiziNutrisiAdapter(Context mContext, List<GiziNutrisi> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public GiziNutrisiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.gizi_nutrisi_cardview, parent, false);
        return new GiziNutrisiHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GiziNutrisiHolder holder, int position) {

        if (mData.get(position).getCardStyle() != 0) {
            holder.giziNutrisiCard.setBackgroundResource(mData.get(position).getCardStyle());
        }
        holder.titleText.setText(mData.get(position).getTitleAgeReq());
        holder.captionText.setText(mData.get(position).getCaptionTitle());
        holder.attachedImg.setImageResource(mData.get(position).getAttachedImg());
        holder.giziNutrisiCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailedGiziNutrisi.class);
                intent.putExtra("Title", mData.get(position).getTitleAgeReq());
                intent.putExtra("posisi", position);
                intent.putExtra("headerImg", mData.get(position).getHeaderImg());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class GiziNutrisiHolder extends RecyclerView.ViewHolder {

        TextView titleText;
        TextView captionText;
        CardView giziNutrisiCard;
        FloatingActionButton attachedImg;

        public GiziNutrisiHolder(@NonNull View itemView) {
            super(itemView);

            titleText = (TextView) itemView.findViewById(R.id.nutrisi_gizi_title);
            captionText = (TextView) itemView.findViewById(R.id.nutrisi_gizi_caption);
            giziNutrisiCard = (CardView) itemView.findViewById(R.id.gizi_nutrisi_card);
            attachedImg = (FloatingActionButton) itemView.findViewById(R.id.floating_image);
        }
    }
}
