package com.example.babegue.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.babegue.R;
import com.example.babegue.data.model.Anak;
import com.example.babegue.helper.ShowAddAnakDialog;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class ProfileAnakAdapter extends RecyclerView.Adapter<ProfileAnakAdapter.ProfileAnakHolder>{

    private Context mContext;
    private List<Anak> mData;

    public ProfileAnakAdapter(Context mContext, List<Anak> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ProfileAnakHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.profile_anak_card, parent, false);
        return new ProfileAnakHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileAnakHolder holder, int position) {
        holder.anakCard.setBackgroundResource(R.drawable.fb_messanger);
        holder.namaAnak.setText(mData.get(position).getNamaAnak());
        holder.tglAnak.setText(mData.get(position).getTlAnak());
        if (mData.get(position).getjKelamin().equals("L")) {
            holder.jkAnak.setText("Laki - Laki");
        } else if (mData.get(position).getjKelamin().equals("P")) {
            holder.jkAnak.setText("Perempuan");
        }

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAddAnakDialog.updateDialog(R.layout.form_anak, LayoutInflater.from(mContext),
                        false, mData.get(position).getIdAnak(), mData.get(position));
            }
        });

        FirebaseDatabase.getInstance().getReference("DataBabies").child("Babies")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        notifyDataSetChanged();
                    }
                });

//        FirebaseDatabase.getInstance().getReference("DataBabies").child("Babies")
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        notifyItemChanged(position);
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ProfileAnakHolder extends RecyclerView.ViewHolder{

        private CardView anakCard;
        private TextView namaAnak;
        private TextView tglAnak;
        private TextView jkAnak;
        private Button editButton;

        public ProfileAnakHolder(@NonNull View itemView) {
            super(itemView);
            anakCard = (CardView) itemView.findViewById(R.id.anak_card);
            namaAnak = (TextView) itemView.findViewById(R.id.nama_anak);
            tglAnak = (TextView) itemView .findViewById(R.id.tgl_anak);
            jkAnak = (TextView) itemView.findViewById(R.id.jk_anak);
            editButton = (Button) itemView.findViewById(R.id.edit_button);
        }
    }
}
