package com.example.babegue.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.babegue.R;
import com.example.babegue.data.model.NotificationModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationsHolder>{

    private Context mContext;
    private List<NotificationModel> listNotif;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private TextView notFound;

    public NotificationAdapter(Context mContext, List<NotificationModel> listNotif, TextView notifNotFound) {
        this.mContext = mContext;
        this.listNotif = listNotif;
        this.notFound = notifNotFound;

        databaseReference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
    }

    @NonNull
    @Override
    public NotificationsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.notif_cardview, parent, false);
        return new NotificationsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsHolder holder, int position) {
//        if (listNotif.size() < 1) {
//            notFound.setVisibility(View.VISIBLE);
//        }
        Date now = new Date();

        holder.notifCard.setBackgroundResource(R.drawable.notification_card_back);
        holder.notifTitle.setText(listNotif.get(position).getTitle());
        holder.notifMessage.setText(listNotif.get(position).getMessage());

        Long clock = listNotif.get(position).getTimestamp();
        Long diff = now.getTime() - clock;
        holder.notifClock.setText(toTime(diff));

        holder.closeButton.setOnClickListener(v -> {
            Query query = databaseReference.child("DataNotifications").child("Notifications")
                    .orderByChild("timestamp").equalTo(clock);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        ds.getRef().removeValue().addOnCompleteListener(task -> {
                            listNotif.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, listNotif.size());
                            if (listNotif.size() < 1) {
                                notFound.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        });
    }

    private String toTime(Long mili) {
        int time = (int) (mili / (1000 % 60));
        String clock = time+" detik";
        if (time > 59) {
            time = (int) (mili / (60 * 1000) % 60);
            clock = (time+1)+" menit";
            if (time > 59) {
                time = (int) (mili / (60 * 60 * 1000));
                clock = (time+1)+" jam";
                if (time > 23) {
                    time = (int) (mili / (60*60*24*1000));
                    clock = (time+1)+" hari";
                }
            }
        }
        return clock+" yang lalu";
    }

    @Override
    public int getItemCount() {
        return listNotif.size();
    }

    public static class NotificationsHolder extends RecyclerView.ViewHolder {

        private CardView notifCard;
        private TextView notifTitle;
        private TextView notifMessage;
        private TextView notifClock;
        private FloatingActionButton closeButton;

        public NotificationsHolder(@NonNull View itemView) {
            super(itemView);
            notifCard = (CardView) itemView.findViewById(R.id.notif_card);
            notifTitle = (TextView) itemView.findViewById(R.id.notif_title);
            notifMessage = (TextView) itemView.findViewById(R.id.notif_message);
            notifClock = (TextView) itemView.findViewById(R.id.notif_clock);
            closeButton = (FloatingActionButton) itemView.findViewById(R.id.button_close);
        }
    }
}
