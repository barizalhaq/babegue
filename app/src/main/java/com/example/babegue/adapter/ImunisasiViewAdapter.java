package com.example.babegue.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.example.babegue.R;
import com.example.babegue.data.model.Imunisasi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class ImunisasiViewAdapter extends RecyclerView.Adapter<ImunisasiViewAdapter.ImunisasiHolder> {

    private Context mContext;
    private List<Imunisasi> mData;
    private String idAnak;

    public ImunisasiViewAdapter(Context mContext, List<Imunisasi> mData, String idAnak) {
        this.mContext = mContext;
        this.mData = mData;
        this.idAnak = idAnak;
        Log.d("IDANAK", idAnak);
    }

    @NonNull
    @Override
    public ImunisasiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.expandable_cardview, parent, false);

        return new ImunisasiHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImunisasiHolder holder, int position) {

        String[] imunisasiAll = mData.get(position).getNameImunisasi();
        FirebaseDatabase.getInstance().getReference("DataImunisasi").child("Imunisasi")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(idAnak)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            String imunisasi = dataSnapshot.getValue().toString();
                            if (imunisasi.contains(imunisasiAll[0])) {
                                holder.firstCheckbox.setEnabled(false);
                                holder.firstCheckbox.setChecked(true);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        if (imunisasiAll.length > 1) {
            holder.secondCheckbox.setVisibility(View.VISIBLE);
            holder.imunisasiSecondName.setVisibility(View.VISIBLE);
            holder.imunisasiSecondName.setText(imunisasiAll[1]);
            FirebaseDatabase.getInstance().getReference("DataImunisasi").child("Imunisasi")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(idAnak)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                String imunisasi = dataSnapshot.getValue().toString();
                                if (imunisasi.contains(imunisasiAll[1])) {
                                    holder.firstCheckbox.setEnabled(false);
                                    holder.firstCheckbox.setChecked(true);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
            holder.secondCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                Imunisasi imunisasi = new Imunisasi();
                if (holder.secondCheckbox.isEnabled()) {
                    if (isChecked) {
                        imunisasi.setTitleImunisasi(imunisasiAll[1]);
                        showDialog(imunisasiAll[1], imunisasi, holder.secondCheckbox);
                    }
                }

            });
        } else {
            holder.secondCheckbox.setVisibility(View.GONE);
            holder.imunisasiSecondName.setVisibility(View.GONE);
        }

        holder.imunisasiTitle.setText(mData.get(position).getTitleImunisasi());
        holder.imunisasiName.setText(imunisasiAll[0]);
        holder.imunisasiCard.setBackgroundResource(R.drawable.imunisasi_bg_first);

        holder.firstCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {

            Imunisasi imunisasi = new Imunisasi();
            if (holder.firstCheckbox.isEnabled()) {
                if (isChecked) {
                    imunisasi.setTitleImunisasi(imunisasiAll[0]);
                    showDialog(imunisasiAll[0], imunisasi, holder.firstCheckbox);
                }
            }
        });

        holder.buttonExpand.setOnClickListener(v -> {
            if (holder.expendableView.getVisibility() == View.GONE) {
                TransitionManager.beginDelayedTransition(holder.imunisasiCard, new AutoTransition().setDuration(250));
                holder.expendableView.setVisibility(View.VISIBLE);
//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.expendableView.getLayoutParams();
//                    params.setMarginEnd(20);
//                    holder.expendableView.setLayoutParams(params);

                holder.buttonExpand.setBackgroundResource(R.drawable.button_expand_false);
            } else if (holder.expendableView.getVisibility() == View.VISIBLE) {
                TransitionManager.beginDelayedTransition(holder.imunisasiCard, new AutoTransition().setDuration(20));
                holder.expendableView.setVisibility(View.GONE);
                holder.buttonExpand.setBackgroundResource(R.drawable.button_expand_background);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void showDialog(String nama, Imunisasi imunisasi, AppCompatCheckBox holder) {
        AlertDialog.Builder confirm = new AlertDialog.Builder(mContext);
        confirm.setTitle("Konfirmasi")
                .setMessage("Sudahkah buah hati menerima imunisasi "+nama+"?\n" +
                        "\n Aksi ini tidak bisa diubah.")
                .setPositiveButton("Lanjutkan", (dialog, which) -> {
                    FirebaseDatabase.getInstance().getReference("DataImunisasi").child("Imunisasi")
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child(idAnak)
                            .push().setValue(imunisasi)
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    holder.setEnabled(false);
                                }
                            });
                    dialog.dismiss();
                });
        confirm.setNegativeButton("Batalkan", (dialog, which) -> {
            holder.setChecked(false);
            dialog.dismiss();
        });

        confirm.show();
    }

    public static class ImunisasiHolder extends RecyclerView.ViewHolder {

        TextView imunisasiTitle;
        TextView imunisasiName;
        TextView imunisasiSecondName;
        CardView imunisasiCard;
        Button buttonExpand;
        RelativeLayout expendableView;
        AppCompatCheckBox firstCheckbox;
        AppCompatCheckBox secondCheckbox;

        public ImunisasiHolder(View view) {
            super(view);

            imunisasiTitle = (TextView) view.findViewById(R.id.imunisasi_title);
            imunisasiName = (TextView) view.findViewById(R.id.expandable_text);
            imunisasiSecondName = (TextView) view.findViewById(R.id.expandable_text_second);
            imunisasiCard = (CardView) view.findViewById(R.id.card_imunisasi_template);
            buttonExpand = (Button) view.findViewById(R.id.button_expand_imunisasi);
            expendableView = (RelativeLayout) view.findViewById(R.id.expandable_view);
            firstCheckbox = (AppCompatCheckBox) view.findViewById(R.id.first_checkbox);
            secondCheckbox = (AppCompatCheckBox) view.findViewById(R.id.second_checkbox);
        }
    }
}
