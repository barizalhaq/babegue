package com.example.babegue.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.babegue.DetailedPanduanAnak;
import com.example.babegue.R;
import com.example.babegue.data.model.PanduanAnak;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class PanduanAnakAdapter extends RecyclerView.Adapter<PanduanAnakAdapter.PanduanAnakHolder> {

    private Context mContext;
    private List<PanduanAnak> mData;

    public PanduanAnakAdapter(Context mContext, List<PanduanAnak> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public PanduanAnakHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.panduan_anak_cardview, parent, false);
        return new PanduanAnakHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PanduanAnakHolder holder, int position) {
        if (mData.get(position).getCardStyle() != 0) {
            holder.cardView.setBackgroundResource(mData.get(position).getCardStyle());
        }
        holder.title.setText(mData.get(position).getTitle());
        holder.caption.setText(mData.get(position).getDesc());
        holder.attachedImg.setImageResource(mData.get(position).getHeaderImg());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailedPanduanAnak.class);
                intent.putExtra("Title", mData.get(position).getTitle());
                intent.putExtra("posisi", position);
                intent.putExtra("headerImg", mData.get(position).getHeaderImg());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class PanduanAnakHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView title;
        TextView caption;
        FloatingActionButton attachedImg;

        public PanduanAnakHolder(@NonNull View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.panduan_anak_card);
            title = (TextView) itemView.findViewById(R.id.panduan_anak_title);
            caption = (TextView) itemView.findViewById(R.id.panduan_anak_caption);
            attachedImg = (FloatingActionButton) itemView.findViewById(R.id.floating_image);
        }
    }
}
