package com.example.babegue.callback;

import java.util.List;

public interface NotificationListCallback {
    void onCallback(List<String> idNotification);
}
