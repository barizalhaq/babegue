package com.example.babegue.callback;

import com.example.babegue.data.model.Anak;

import java.util.List;

public interface AnakListCallback {
    void onCallback(List<String> idAnak);
}
