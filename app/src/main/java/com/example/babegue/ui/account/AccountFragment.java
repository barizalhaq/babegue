package com.example.babegue.ui.account;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.babegue.LoginActivity;
import com.example.babegue.adapter.ProfileAnakAdapter;
import com.example.babegue.R;
import com.example.babegue.callback.AnakListCallback;
import com.example.babegue.data.model.Anak;
import com.example.babegue.data.model.User;
import com.example.babegue.helper.ShowAddAnakDialog;
import com.example.babegue.helper.ShowEditProfileDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ramotion.cardslider.CardSliderLayoutManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccountFragment extends Fragment {

    private FloatingActionButton addAnak;
    private AlertDialog.Builder addAnakDialog;

    private TextView namaOrtu;
    private TextView emailUser;
    private Button ubahProfile;
    private Button logout;

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    private String userId;

    private List<Anak> listAnak;
    private RecyclerView recyclerView;

    private List<String> idAnak;
    private String[] splitted_end;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account, container, false);

        mAuth = FirebaseAuth.getInstance();

        addAnak = (FloatingActionButton) view.findViewById(R.id.fab_add);
        addAnak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(inflater);
            }
        });

        initialize(view);
        setProfileAnak(view);

        logout.setOnClickListener(v -> {
            AlertDialog.Builder confirm = new AlertDialog.Builder(v.getContext());
            confirm.setTitle("Konfirmasi")
                    .setMessage("Apakah anda yakin ingin keluar?")
                    .setPositiveButton("Keluar", (dialog, which) -> {
                        dialog.dismiss();
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    });
            confirm.setNegativeButton("Batalkan", (dialog, which) -> dialog.dismiss());
            confirm.show();
        });

        ubahProfile.setOnClickListener(v -> ShowEditProfileDialog.showDialog(inflater));

        return view;
    }

    private void showDialog(LayoutInflater inflater) {
        ShowAddAnakDialog.showDialog(R.layout.form_anak, inflater, false, getActivity());
    }

    private void initialize(View view) {
        namaOrtu = (TextView) view.findViewById(R.id.prof_nama);
        emailUser = (TextView) view.findViewById(R.id.prof_email);
        ubahProfile = (Button) view.findViewById(R.id.ubah_profile);
        logout = (Button) view.findViewById(R.id.logout);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        listAnak = new ArrayList<>();
        idAnak = new ArrayList<>();

        setProfileCred();
    }

    private void setProfileCred() {
        firebaseUser = mAuth.getCurrentUser();
        userId = firebaseUser.getUid();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                showProfileData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        emailUser.setText(firebaseUser.getEmail());
    }

    private void showProfileData(DataSnapshot dataSnapshot) {
        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            User userInfo = new User();
            userInfo.setFullName(dataSnapshot.child("DataUsers").child("Users").child(userId)
                    .getValue(User.class).getFullName());
            userInfo.setEmail(dataSnapshot.child("DataUsers").child("Users").child(userId)
                    .getValue(User.class).getEmail());

            namaOrtu.setText(userInfo.getFullName());
            emailUser.setText(userInfo.getEmail());
        }
    }

    private void setProfileAnak(View view) {
        getIdAnak(new AnakListCallback() {
            @Override
            public void onCallback(List<String> idAnak) {
                for (int i = 0; i < idAnak.size(); i++) {
                    addListAnak(idAnak.get(i));
                }
            }
        });
    }


    private void getIdAnak(AnakListCallback anakListCallback) {
        databaseReference.child("DataParents").child("Parents").child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            String idAnak = dataSnapshot.getValue().toString();
                            idAnak = idAnak.substring(1, idAnak.length()-1);
                            String[] splitted = idAnak.split(",");
                            splitted_end = Arrays.copyOf(splitted, splitted.length);
                        }
                        if (splitted_end != null) {
                            for (int i = 0; i < splitted_end.length; i++) {
                                splitted_end[i] = splitted_end[i].substring(splitted_end[i].lastIndexOf("{")+1,
                                        splitted_end[i].lastIndexOf("="));
                                idAnak.add(splitted_end[i]);
                            }
                        }
                        anakListCallback.onCallback(idAnak);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void addListAnak(String id) {
        databaseReference.child("DataBabies").child("Babies").child(id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Anak anak = new Anak();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            anak.setNamaAnak(dataSnapshot.getValue(Anak.class).getNamaAnak());
                            anak.setTlAnak(dataSnapshot.getValue(Anak.class).getTlAnak());
                            anak.setjKelamin(dataSnapshot.getValue(Anak.class).getjKelamin());
                            anak.setIdAnak(dataSnapshot.getKey());
                        }
                        setAnak(anak);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
//
    }

    private void setAnak(Anak anak) {
        listAnak.add(new Anak(anak.getNamaAnak(), anak.getTlAnak(), anak.getjKelamin(),
                anak.getIdAnak()));
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.prof_anak_template);
        ProfileAnakAdapter profileAnakAdapter = new ProfileAnakAdapter(getActivity(), listAnak);
        recyclerView.setLayoutManager(new CardSliderLayoutManager(50, 600, 50));

        recyclerView.setAdapter(profileAnakAdapter);
//        new CardSnapHelper().attachToRecyclerView(recyclerView);
    }
}
