package com.example.babegue.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.babegue.adapter.NotificationAdapter;
import com.example.babegue.R;
import com.example.babegue.callback.NotificationListCallback;
import com.example.babegue.data.model.NotificationModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class NotificationsFragment extends Fragment {

    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private String userId;

    private List<String> idNotif;
    private String[] splitted_end;

    private List<NotificationModel> notificationModelList;

    private TextView notifNotFound;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        notifNotFound = (TextView) root.findViewById(R.id.notif_notfound);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        userId = mUser.getUid();

        idNotif = new ArrayList<>();
        notificationModelList = new ArrayList<>();
        getNotif(root);

        return root;
    }

    private void getNotif(View view) {
        getIdNotification(idNotification -> {
            for (int i = 0; i < idNotification.size(); i++) {
                getData(idNotification.get(i));
            }
        });
    }

    private void getIdNotification(NotificationListCallback notificationListCallback) {
        databaseReference.child("UserNotifications").child("Notifications").child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            String idNotification = dataSnapshot.getValue().toString();
                            idNotification = idNotification.substring(1, idNotification.length()-1);
                            String[] splitted = idNotification.split(",");
                            splitted_end = Arrays.copyOf(splitted, splitted.length);
                        }
                        if (splitted_end != null) {
                            for (int i = 0; i < splitted_end.length; i++) {
                                splitted_end[i] = splitted_end[i].substring(splitted_end[i].lastIndexOf("{")+1,
                                        splitted_end[i].lastIndexOf("="));
                                idNotif.add(splitted_end[i]);
                            }
                        } else {
                            notifNotFound.setVisibility(View.VISIBLE);
                        }
                        notificationListCallback.onCallback(idNotif);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getData(String id) {
        databaseReference.child("DataNotifications").child("Notifications").child(id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        NotificationModel notif = new NotificationModel();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            notif.setTitle(dataSnapshot.getValue(NotificationModel.class).getTitle());
                            notif.setMessage(dataSnapshot.getValue(NotificationModel.class).getMessage());
                            notif.setTimestamp(dataSnapshot.getValue(NotificationModel.class).getTimestamp());
                            notif.setActive(dataSnapshot.getValue(NotificationModel.class).getActive());
                        }
                        setNotif(notif);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void setNotif(NotificationModel notif) {
        if (notif.getActive() != null && notif.getActive()) {
            notificationModelList.add(new NotificationModel(notif.getTitle(), notif.getMessage(), notif.getNamaAnak(),
                    notif.getTimestamp(), notif.getActive()));
        }
        RecyclerView rcView = (RecyclerView) Objects.requireNonNull(getActivity()).findViewById(R.id.notif_template);
        NotificationAdapter notificationAdapter = new NotificationAdapter(getActivity(),
                notificationModelList, notifNotFound);

        rcView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        rcView.setAdapter(notificationAdapter);
    }

}