package com.example.babegue.ui.dashboard;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.babegue.DiagnosaActivity;
import com.example.babegue.GiziNutrisiActivity;
import com.example.babegue.PanduanAnakActivity;
import com.example.babegue.PertumbuhanActivity;
import com.example.babegue.R;
import com.example.babegue.ImunisasiActivity;

import static androidx.core.content.ContextCompat.getSystemService;

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private DashboardViewModel dashboardViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        CardView cardDiagnosa = (CardView) root.findViewById(R.id.card_diagnosa);
        cardDiagnosa.setOnClickListener(this);
        CardView cardGizi = (CardView) root.findViewById(R.id.card_gizi);
        cardGizi.setOnClickListener(this);
        CardView cardImunisasi = (CardView) root.findViewById(R.id.card_imunisasi);
        cardImunisasi.setOnClickListener(this);
        CardView cardPertumbuhan = (CardView) root.findViewById(R.id.card_pertumbuhan);
        cardPertumbuhan.setOnClickListener(this);
        CardView cardPerkembangan = (CardView) root.findViewById(R.id.card_perkembangan);
        cardPerkembangan.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_diagnosa:
                startActivity(new Intent(getActivity(), DiagnosaActivity.class));
                break;
            case R.id.card_gizi:
                startActivity(new Intent(getActivity(), GiziNutrisiActivity.class));
                break;
            case R.id.card_imunisasi:
                startActivity(new Intent(getActivity(), ImunisasiActivity.class));
                break;
            case R.id.card_perkembangan:
                startActivity(new Intent(getActivity(), PanduanAnakActivity.class));
                break;
            case R.id.card_pertumbuhan:
                startActivity(new Intent(getActivity(), PertumbuhanActivity.class));
                break;
        }
    }
}