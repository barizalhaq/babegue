package com.example.babegue.helper;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

import com.example.babegue.DiagnosaActivity;
import com.example.babegue.R;

import java.util.Objects;

public class ShowDiagnosaResult {

    private static String content;

    public static void showDialog(LayoutInflater layoutInflater, int content) {
        View view = layoutInflater.inflate(R.layout.diagnosa_dialog, null);
        AlertDialog.Builder diagnosaDialog = new AlertDialog.Builder(view.getContext());
        diagnosaDialog.setView(view);

        Button closeButton = (Button) view.findViewById(R.id.close_button);

        final AlertDialog dialog = diagnosaDialog.create();
        dialog.setCancelable(false);

        Objects.requireNonNull(dialog.getWindow()).
                setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

       setResultView(view, content);

       closeButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               dialog.dismiss();
           }
       });

        dialog.show();
    }

    public static void setResultView(View view, int content) {
        WebView resultView = (WebView) view.findViewById(R.id.result);
        WebSettings webSettings = resultView.getSettings();
//        resultView.setBackgroundColor(0);
//        webSettings.setDefaultFontSize(28);
        resultView.loadData(view.getResources().getString(content),
                "text/html; charset=utf-8", "UTF-8");
    }

}
