package com.example.babegue.helper;

import androidx.annotation.NonNull;

import com.example.babegue.data.model.Anak;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class AddAnak {

    private interface UpdateData {
        void onCallback(boolean result);
    }

    private static Anak dataAnak;
    private static String key;
    private static FirebaseDatabase firebaseDatabase;
    private static DatabaseReference databaseReference;

    private static FirebaseAuth mAuth;
    private static FirebaseUser firebaseUser;

    private static String parentUID;

    private static String idAnak;
    private static boolean resultUpdate = false;

    public static boolean storeAnak(Anak dataAnak) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("DataBabies");

        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        parentUID = firebaseUser.getUid();

        key = databaseReference.push().getKey();
        databaseReference.child("Babies").child(key).setValue(dataAnak)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                        }
                    }
                });
        if (attachParent(key, parentUID)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean updateAnak(Anak data, String id) {
        idAnak = id;
        dataAnak = data;
        update(result -> resultUpdate = result);
        return true;
    }

    private static void update(UpdateData updateData) {
        FirebaseDatabase.getInstance().getReference("DataBabies").child("Babies")
                .child(idAnak).setValue(dataAnak).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    updateData.onCallback(true);
                } else {
                    updateData.onCallback(false);
                }
            }
        });
    }

    public static boolean attachParent(String babyKey, String parentUID) {
        Map<String, Boolean> attached = new HashMap<>();
        databaseReference = firebaseDatabase.getReference("DataParents");

        attached.put(babyKey, true);

        databaseReference.child("Parents").child(parentUID).push().setValue(attached);

        return true;
    }
}
