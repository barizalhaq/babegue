package com.example.babegue.helper;

import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.example.babegue.R;
import com.example.babegue.data.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ShowEditProfileDialog {

    private static AlertDialog.Builder dialogEdit;

    private static EditText inputNama;
    private static EditText email;
//    private static EditText password;
//    private static EditText password_conf;
    private static Button update;
    private static Button cancel;

    private static User user;

    public static void showDialog(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.dialog_edit_profile, null);
        dialogEdit = new AlertDialog.Builder(view.getContext());
        dialogEdit.setView(view);
        dialogEdit.setTitle("Ubah Profile");

        inputNama = (EditText) view.findViewById(R.id.nama);
        email = (EditText) view.findViewById(R.id.email);
        update = (Button) view.findViewById(R.id.button_submit);
        cancel = (Button) view.findViewById(R.id.button_cancel);

        FirebaseDatabase.getInstance().getReference("DataUsers").child("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        user = new User();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            Log.d("DATAUSER", dataSnapshot.getValue().toString());
                            user.setFullName(dataSnapshot.getValue(User.class).getFullName());
                            user.setEmail(dataSnapshot.getValue(User.class).getEmail());
                        }
                        inputNama.setText(user.getFullName());
                        email.setText(user.getEmail());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        final AlertDialog dialog = dialogEdit.create();

        update.setOnClickListener(v -> {
            if (submitDialog(v)) {
                dialog.dismiss();
            } else {
            }
        });

        cancel.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }

    private static boolean submitDialog(View view) {
        String nama = inputNama.getText().toString();
        String userMail = email.getText().toString();

        if (false) {
            return false;
        } else {
            user = new User(nama, userMail);

            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser fUser = mAuth.getCurrentUser();

//        AuthCredential authCredential = EmailAuthProvider.getCredential(fUser.getEmail(), fUser.)

            if (fUser != null) {
                fUser.updateEmail(userMail).addOnCompleteListener(task -> {
                    try {
                        FirebaseDatabase.getInstance().getReference("DataUsers").child("Users")
                                .child(fUser.getUid()).setValue(user);
                    } catch (Exception e) {

                    }

                });
            }
            return true;
        }
    }

}
