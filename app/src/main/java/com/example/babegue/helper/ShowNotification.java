package com.example.babegue.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.babegue.NavigationActivity;
import com.example.babegue.R;
import com.example.babegue.data.model.NotificationModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.babegue.Baymax.CHANNEL_1_ID;

public class ShowNotification {

    private static NotificationManagerCompat notificationManager;
    private static DatabaseReference databaseReference;
    private static FirebaseAuth mAuth;
    private static FirebaseUser mUser;

    public static void showNotification(Context context, String type, String namaAnak) {
        notificationManager = NotificationManagerCompat.from(context);
        Intent intent = new Intent(context, NavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.putExtra("NotificationModel", 1);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                0);

        Notification notification = new NotificationCompat.Builder(context, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.imunisasi)
                .setContentTitle("Peringatan Imunisasi")
                .setContentText(message(type, namaAnak))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
        notificationManager.notify(1, notification);
        storeNotification(type, namaAnak);
    }

    private static String message(String type, String namaAnak) {
        String message = "Jangan lupa untuk memberi buah hati "+namaAnak+" imunisasi "+type;
        return message;
//        switch (type) {
//            case "HBO":
//                return main+"HBO";
//                break;
//            case "BCG"
//                return main+
//        }
    }

    private static void storeNotification(String type, String namaAnak) {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        Date timestamp = new Date();
        Map<String, Boolean> userNotif = new HashMap<>();
        NotificationModel notificationModel = new NotificationModel(type, message(type, namaAnak), namaAnak,
                timestamp.getTime(), null);
        String key = databaseReference.push().getKey();
        databaseReference.child("DataNotifications").child("Notifications").child(key)
                .setValue(notificationModel);

        // Add relation
        userNotif.put(key, true);
        databaseReference.child("UserNotifications").child("Notifications").child(mUser.getUid())
                .push().setValue(userNotif);
    }

}
