package com.example.babegue.helper;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;

import com.example.babegue.callback.AnakListCallback;
import com.example.babegue.data.model.Anak;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TimeToImunisasi {

    private static FirebaseAuth mAuth;
    private static FirebaseUser mUser;
    private static DatabaseReference databaseReference;
    private List<String> idAnak;
    private String[] splitted_end;
    private List<Anak> dataAnak;
    private Context context;

    public TimeToImunisasi(Context context) {
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        idAnak = new ArrayList<>();
        dataAnak = new ArrayList<>();
        this.context = context;
    }

    public void checkTimeToImunisasi() {
        getIdAnak(new AnakListCallback() {
            @Override
            public void onCallback(List<String> idAnak) {
                for (int i = 0; i < idAnak.size(); i++) {
                    getDataAnak(idAnak.get(i));
                }
            }
        });
    }

    public void getIdAnak(final AnakListCallback anakListCallback) {
        databaseReference.child("DataParents").child("Parents").child(mUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            String dataAnak = dataSnapshot.getValue().toString();
                            dataAnak = dataAnak.substring(1, dataAnak.length()-1);
                            String[] splitted = dataAnak.split(",");
                            splitted_end = Arrays.copyOf(splitted, splitted.length);
                        }
                        if (splitted_end != null) {
                            for (int i = 0; i < splitted_end.length; i++) {
                                splitted_end[i] = splitted_end[i].substring(splitted_end[i].lastIndexOf("{")+1,
                                        splitted_end[i].lastIndexOf("="));
                                idAnak.add(splitted_end[i]);
                            }
                        }
                        anakListCallback.onCallback(idAnak);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public void getDataAnak(String id) {
            databaseReference.child("DataBabies").child("Babies").child(id)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Anak anak = new Anak();
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                anak.setNamaAnak(dataSnapshot.getValue(Anak.class).getNamaAnak());
                                anak.setTlAnak(dataSnapshot.getValue(Anak.class).getTlAnak());
                            }
                            String namaAnak = anak.getNamaAnak();
                            String tglLahir = anak.getTlAnak();
                            checkDate(tglLahir, namaAnak);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
    }

    public void checkDate(String tglLahir, String namaAnak) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date now = new Date();

        Date birthD = null;
        try {
            birthD = new SimpleDateFormat("dd-MM-yyyy").parse(tglLahir);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Long different  = now.getTime() - birthD.getTime();

        checkImunisasiType(toDay(different), namaAnak);

//        Calendar c = Calendar.getInstance();
//
//        c.setTime(birthD);
//        c.add(Calendar.DATE, 1);
    }

    public int toDay(Long mili) {
//        SimpleDateFormat form = new SimpleDateFormat("dd");
//        Date diff = new Date(mili);
//        return Integer.valueOf(form.format(diff));
        return (int) (mili / (60*60*24*1000));
    }

    public void checkImunisasiType(int age, String namaAnak) {
        if (age <= 7) {
            ShowNotification.showNotification(context, "HBO", namaAnak);
        } else if (age > 7 && age <= 30) {
            ShowNotification.showNotification(context, "BCG, Polio 1", namaAnak);
        } else if (age > 31 && age <= 60) {
            ShowNotification.showNotification(context, "DPT-HB-Hib 1, Polio 2", namaAnak);
        } else if (age > 61 && age <= 90) {
            ShowNotification.showNotification(context, "DPT-HB-Hib 2, Polio 3", namaAnak);
        } else if (age > 91 && age <= 120) {
            ShowNotification.showNotification(context, "DPT-HB-Hib 3, Polio 4", namaAnak);
        } else if (age > 121 && age <= 270) {
            ShowNotification.showNotification(context, "Campak", namaAnak);
        } else if (age > 271 && age <= 300) {
            ShowNotification.showNotification(context, "DPT-HB-Hib lanjutan, Campak lanjutan", namaAnak);
        }
    }
}
