package com.example.babegue.helper;

import android.widget.Toast;

import com.example.babegue.DiagnosaActivity;
import com.example.babegue.R;

import java.util.HashMap;
import java.util.Map;

public class DiagnosaAnak {

//    private static Map<String, Boolean> hasilDiagnosa;

    public static void runDiagnosaBatuk(Map<String, Boolean> hasilDiagnosa, DiagnosaActivity diagnosaActivity) {
        if (hasilDiagnosa.get("G03") || hasilDiagnosa.get("G04") || hasilDiagnosa.get("G13")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S01);
        } else if (hasilDiagnosa.get("G14")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S03);
        } else if (!hasilDiagnosa.get("G14")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S02);
        }
    }

    public static void runDiagnosaDiare(Map<String, Boolean> hasilDiagnosa, DiagnosaActivity diagnosaActivity) {
        if (hasilDiagnosa.get("G03") || hasilDiagnosa.get("G05") || hasilDiagnosa.get("G06")
        || hasilDiagnosa.get("G07")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S01);
        } else if (hasilDiagnosa.get("G14")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S05);
        } else if (!hasilDiagnosa.get("G14")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S04);
        }
    }

    public static void runDiagnosaDemam(Map<String, Boolean> hasilDiagnosa, DiagnosaActivity diagnosaActivity) {
        if (hasilDiagnosa.get("G01") || hasilDiagnosa.get("G02")
                || hasilDiagnosa.get("G08") || hasilDiagnosa.get("G09") || hasilDiagnosa.get("G10")
                || hasilDiagnosa.get("G11") || hasilDiagnosa.get("G13")) {
            ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S01);
        } else if (!hasilDiagnosa.get("G12")) {
            if (hasilDiagnosa.get("G14")) {
                ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S09);
            } else if (!hasilDiagnosa.get("G14")) {
                ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S08);
            }
        } else if (hasilDiagnosa.get("G12")) {
            if (hasilDiagnosa.get("G14")) {
                ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S07);
            } else if (!hasilDiagnosa.get("G14")) {
                ShowDiagnosaResult.showDialog(diagnosaActivity.getLayoutInflater(), R.string.S06);
            }
        }
    }

}
