package com.example.babegue.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.babegue.R;
import com.example.babegue.data.model.Anak;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ShowAddAnakDialog {

    private static AlertDialog.Builder dialogAddAnak;

    private static EditText namaAnak;
    private static DatePicker tglLahir;
    private static Button submitButton;
    private static Button cancelButton;
    private static RadioGroup jenisKelamin;
    private static String kelaminAnak;

    private static Date date;
//    private static AwesomeValidation awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

    public static void showDialog(int layout, LayoutInflater inflater, boolean firstTime,
                                  Activity activity) {
        View view = inflater.inflate(layout, null);
        dialogAddAnak = new AlertDialog.Builder(view.getContext());
        dialogAddAnak.setView(view);
        dialogAddAnak.setTitle("Biodata Anak");

//        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
//        FormValidator.validate((Activity) inflater.getContext(), new SimpleErrorPopupCallback(inflater.getContext()));

        namaAnak = view.findViewById(R.id.input_nama_anak);
        tglLahir = view.findViewById(R.id.tanggal_lahir);
        jenisKelamin = view.findViewById(R.id.jenis_kelamin);
        submitButton = view.findViewById(R.id.submit_button);
        cancelButton = view.findViewById(R.id.cancel_button);

//        awesomeValidation.addValidation(activity, R.id.input_nama_anak, Patterns.EMAIL_ADDRESS,
//                R.string.fullname_err);

        final AlertDialog alertDialog = dialogAddAnak.create();
        alertDialog.setCancelable(false);

        if (firstTime) {
            cancelButton.setVisibility(View.GONE);
        }

        jenisKelamin.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.laki_laki) {
                kelaminAnak = "L";
            } else  if (checkedId == R.id.perempuan) {
                kelaminAnak = "P";
            }
        });

        submitButton.setOnClickListener(v -> {
                if (submitDialog(v)) {
                    alertDialog.dismiss();
                } else {
                    Toast.makeText(view.getContext(), "Data Anak Gagal Ditambahkan!",
                            Toast.LENGTH_SHORT).show();
                }
        });

        cancelButton.setOnClickListener(v -> alertDialog.dismiss());

        alertDialog.show();
    }

    public static void updateDialog(int layout, LayoutInflater inflater, boolean firstTime,
                                    String id, Anak anak) {
        View view = inflater.inflate(layout, null);
        dialogAddAnak = new AlertDialog.Builder(view.getContext());
        dialogAddAnak.setView(view);
        dialogAddAnak.setTitle("Ubah Biodata Anak");

        namaAnak = view.findViewById(R.id.input_nama_anak);
        tglLahir = view.findViewById(R.id.tanggal_lahir);
        jenisKelamin = view.findViewById(R.id.jenis_kelamin);
        submitButton = view.findViewById(R.id.submit_button);
        cancelButton = view.findViewById(R.id.cancel_button);

        submitButton.setText("Ubah Data");

        String[] tanggal = anak.getTlAnak().split("-");

        namaAnak.setText(anak.getNamaAnak());
        tglLahir.updateDate(Integer.valueOf(tanggal[2]), Integer.valueOf(tanggal[1])-1,
                Integer.valueOf(tanggal[0]));

        final AlertDialog alertDialog = dialogAddAnak.create();
        alertDialog.setCancelable(false);

        if (firstTime) {
            cancelButton.setVisibility(View.GONE);
        }

        jenisKelamin.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.laki_laki) {
                kelaminAnak = "L";
            } else  if (checkedId == R.id.perempuan) {
                kelaminAnak = "P";
            }
        });

        if (anak.getjKelamin() != null && anak.getjKelamin().equals("L")) {
            jenisKelamin.check(R.id.laki_laki);
        } else {
            jenisKelamin.check(R.id.perempuan);
        }

        submitButton.setOnClickListener(v -> {
            if (updateDialog(v, id)) {
                alertDialog.dismiss();
            } else {
                Toast.makeText(view.getContext(), "Data Anak Gagal Dirubah!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public static boolean submitDialog(View view) {
        if (namaAnak.getText().toString().matches("")) {
            return false;
        }
        if (kelaminAnak == null) {
            return false;
        }
        String _nama = namaAnak.getText().toString();
        String day = Integer.toString(tglLahir.getDayOfMonth());
        String month = Integer.toString(tglLahir.getMonth()+1);
        String year = Integer.toString(tglLahir.getYear());
        String datePick = day+"-"+month+"-"+year;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = dateFormat.parse(datePick);
        } catch (ParseException e) {
        }
        String birthDate = dateFormat.format(date);

        Anak anak = new Anak(_nama, birthDate, kelaminAnak);
        if (AddAnak.storeAnak(anak)) {
            Toast.makeText(view.getContext(), "Data Anak Berhasil Ditambahkan!",
                    Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

    public static boolean updateDialog(View view, String id) {
        if (namaAnak.getText().toString().matches("") || kelaminAnak == null) {
            return false;
        }
        String _nama = namaAnak.getText().toString();
        String day = Integer.toString(tglLahir.getDayOfMonth());
        String month = Integer.toString(tglLahir.getMonth()+1);
        String year = Integer.toString(tglLahir.getYear());
        String datePick = day+"-"+month+"-"+year;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = dateFormat.parse(datePick);
        } catch (ParseException e) {
        }
        String birthDate = dateFormat.format(date);

        Anak anak = new Anak(_nama, birthDate, kelaminAnak);
        if (AddAnak.updateAnak(anak, id)) {
            Toast.makeText(view.getContext(), "Data Anak Berhasil Dirubah!",
                    Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

}
