package com.example.babegue;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class PertumbuhanActivity extends AppCompatActivity {

    private EditText berat;
    private EditText tinggi;
    private Spinner umur;
    private Button submit;

    private float hasil;
    private String indeks;

    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertumbuhan);

        initialize();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitungImt();
            }
        });
    }

    private void initialize() {
        berat = (EditText) findViewById(R.id.berat);
        tinggi = (EditText) findViewById(R.id.tinggi);
        umur = (Spinner) findViewById(R.id.umur);
        submit = (Button) findViewById(R.id.submit_button);

        inflater = LayoutInflater.from(this);
    }

    private void hitungImt() {
        String[] splitted = String.valueOf(umur.getSelectedItem()).split(" ");
        float beratAnak= Float.parseFloat(berat.getText().toString());
        float tinggiAnak= Float.parseFloat(tinggi.getText().toString());
        int umurAnak = Integer.valueOf(splitted[0]);

        tinggiAnak = tinggiAnak / 100;

        hasil = beratAnak / (tinggiAnak * tinggiAnak);

        if (umurAnak == 1) {
            if (hasil <= 13.5) {
                indeks = "Sangat Kurus";
            }  else if (hasil <= 15.45) {
                indeks = "Kurus";
            } else if (hasil <= 19.3) {
                indeks = "Ideal";
            } else if (hasil <= 20) {
                indeks = "Gemuk";
            } else if (hasil > 20) {
                indeks = "Obesitas";
            }
        }

        if (umurAnak == 2) {
            if (hasil <= 14.2) {
                indeks = "Sangat Kurus";
            }  else if (hasil <= 15) {
                indeks = "Kurus";
            } else if (hasil <= 17.5) {
                indeks = "Ideal";
            } else if (hasil <= 18.5) {
                indeks = "Gemuk";
            } else if (hasil > 18.5) {
                indeks = "Obesitas";
            }
        }

        if (umurAnak == 3) {
            if (hasil <= 13.9) {
                indeks = "Sangat Kurus";
            }  else if (hasil <= 14.5) {
                indeks = "Kurus";
            } else if (hasil <= 17.1) {
                indeks = "Ideal";
            } else if (hasil <= 18) {
                indeks = "Gemuk";
            } else if (hasil > 18) {
                indeks = "Obesitas";
            }
        }

        if (umurAnak == 4) {
            if (hasil <= 13.5) {
                indeks = "Sangat Kurus";
            }  else if (hasil <= 14.2) {
                indeks = "Kurus";
            } else if (hasil <= 16.8) {
                indeks = "Ideal";
            } else if (hasil <= 17.6) {
                indeks = "Gemuk";
            } else if (hasil > 17.6) {
                indeks = "Obesitas";
            }
        }

        if (umurAnak == 5) {
            if (hasil <= 13.3) {
                indeks = "Sangat Kurus";
            }  else if (hasil <= 13.9) {
                indeks = "Kurus";
            } else if (hasil <= 16.7) {
                indeks = "Ideal";
            } else if (hasil <= 17.6) {
                indeks = "Gemuk";
            } else if (hasil > 17.6) {
                indeks = "Obesitas";
            }
        }

        showDialog();
    }

    private void showDialog() {
        View view = inflater.inflate(R.layout.imt_result, null);
        AlertDialog.Builder dialogImt = new AlertDialog.Builder(view.getContext());
        dialogImt.setView(view);

        TextView hasilTitle = (TextView) view.findViewById(R.id.hasil);
        TextView hasilDesc = (TextView) view.findViewById(R.id.hasil_desc);
        Button close = (Button) view.findViewById(R.id.close_button);

        hasil = Math.round(hasil);

        hasilTitle.setText(indeks+", IMT: "+Float.toString(hasil));
        hasilDesc.setText(
                "Hasil hitung indeks massa tubuh si buah hati dikategorikan "+indeks+
                        ", dengan IMT sebesar "+Float.toString(hasil)
        );

        final  AlertDialog dialog = dialogImt.create();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
