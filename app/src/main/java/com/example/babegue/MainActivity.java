package com.example.babegue;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    /*
    *   1. Topik atau masalah yang ingin diselesaikan :
    *   - Kekhawatiran ibu muda (baru mempunyai anak) dalam menangani kesehatan anaknya, pertumbuhan dan perkembangan anaknya.
    *   - Beberapa ibu lupa untuk membuka buku kia untuk mengetahui jadwal imunisasi yg harus diberikan kepada anaknya.
    *   - Ibu tidak mengetahui apakah pertumbuhan dan perkembangan anak ideal atau tidak.
    *
    *   2. Mengapa aplikasi tersebut harus dibangun di android?
    *   - Karena aplikasi ini memiliki fitur-fitur yang bisa membantu dan memudahkan para ibu untuk
    *   memantau dan menjaga kesehatan anak-anaknya dalam kesehariannya. Ibu bisa memantau bagaimana
    *   pertumbuhan dan perkembangan anak nya dengan fitur yg tersedia, dan ibu juga bisa menjaga
    *   kesehatan anak nya dengan fitur gizi & nutrisi untuk panduan pemenuhan gizi sesuai umur anaknya
    *   dan fitur diagnosa penyakit untuk mengetahui apa yg harus ibu lakukan ketika kesehatan anaknya
    *   menurun.
    *
    *   3. Siapa user?
    *   - ibu yang memiliki balita
    *
    *   4. Siapa client?
    *   - Tenaga kesehatan untuk balita (bidan,
    *
    *   5. Apa input?
    *   - Gejala-gejala penyakit untuk fitur diagnosa
        - Data balita (Tanggal lahir, berat badan, tinggi badan) untuk fitur pengingat imunisasi, gizi & nutrisi, pertumbuhan.
        - Data pengguna (ibu)
    *
    *   6. Apa output?
    * - Penyakit apa yang menyerang anak
        - Saran apa yang harus dilakukan oleh ibu untuk menangani penyakit anaknya
        - IMT anak
    *
    *   7. Fungsi-fungsi/fitur apa saja yang ada pada aplikasi tersebut?
    *   - Fitur diagnosa penyakit untuk mendiagnosa penyakit apa yg menyerang anak dan memberi saran apa yg harus dilakukan oleh ibu
        - Fitur Imunisasi anak untuk mengingatkan ibu mengenai imunisasi yg harus diberikan kepada anak.
        - Fitur gizi & nutrisi untuk memberi panduan kepada ibu tentang gizi & nutrisi yg harus dipenuhi oleh anaknya sesuai umurnya
        - Fitur pertumbuhan ideal untuk mengetahui pertumbuhan anak apakah ideal atau tidak
        - Fitur panduan perkembangan anak untuk memberi panduan kepada ibu atau orang tua untuk mengajarkan sesuatu agar perkembangan anak nya bagus dan ideal.
    *
    *   DATA APA SAJA YG DI CRUD
        1. AKUN PENGGUNA
        2. DATA BALITA

     * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
