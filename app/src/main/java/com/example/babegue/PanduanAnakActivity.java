package com.example.babegue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.babegue.adapter.PanduanAnakAdapter;
import com.example.babegue.data.model.PanduanAnak;

import java.util.ArrayList;
import java.util.List;

public class PanduanAnakActivity extends AppCompatActivity {

    private List<PanduanAnak> panduanAnak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panduan_anak);

        panduanAnak = new ArrayList<PanduanAnak>();
        panduanAnak.add(new PanduanAnak("PERKEMBANGAN BAYI UMUR 0 - 6 BULAN",
                "", R.drawable.witching_hour, R.drawable.panduan_first));
        panduanAnak.add(new PanduanAnak("PERKEMBANGAN BAYI UMUR 6 - 12 BULAN",
                "", R.drawable.flare, R.drawable.panduan_center));
        panduanAnak.add(new PanduanAnak("PERKEMBANGAN BAYI UMUR 1 - 6 TAHUN",
                "", R.drawable.neuromancer, R.drawable.panduan_last));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.panduan_anak_template);
        PanduanAnakAdapter panduanAnakAdapter = new PanduanAnakAdapter(this, panduanAnak);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(panduanAnakAdapter);
    }
}
