package com.example.babegue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.babegue.adapter.GiziNutrisiAdapter;
import com.example.babegue.data.model.GiziNutrisi;

import java.util.ArrayList;
import java.util.List;

public class GiziNutrisiActivity extends AppCompatActivity {

    private List<GiziNutrisi> giziNutrisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gizi_nutrisi);

        giziNutrisi = new ArrayList<>();
        giziNutrisi.add(new GiziNutrisi("KEBUTUHAN GIZI BAYI UMUR 0 - 6 BULAN",
                "", R.drawable.bluelagoo, R.drawable.third_toddler, R.drawable.baby_stroller));
        giziNutrisi.add(new GiziNutrisi("KEBUTUHAN GIZI BAYI UMUR 6 - 12 BULAN",
                "", R.drawable.cool_blues, R.drawable.second_toddler, R.drawable.duck));
        giziNutrisi.add(new GiziNutrisi("KEBUTUHAN GIZI BAYI UMUR 12 - 24 BULAN",
                "", R.drawable.amin, R.drawable.toddler, R.drawable.electric_car));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.gizi_nutrisi_template);
        GiziNutrisiAdapter giziNutrisiAdapter = new GiziNutrisiAdapter(this, giziNutrisi);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(giziNutrisiAdapter);

    }
}
