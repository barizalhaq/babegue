package com.example.babegue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.babegue.adapter.ImunisasiViewAdapter;
import com.example.babegue.callback.AnakListCallback;
import com.example.babegue.data.model.Anak;
import com.example.babegue.data.model.Imunisasi;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ImunisasiActivity extends AppCompatActivity {

    private List<Imunisasi> listImunisasi;
    private Spinner menuAnak;
    private List<String> idAnak;
    private String[] splitted_end;
    List<String> namaAnak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imunisasi);

        menuAnak = (Spinner) findViewById(R.id.menu_anak);
        idAnak = new ArrayList<>();
        namaAnak = new ArrayList<>();
        listImunisasi = new ArrayList<>();

        getMenuAnak();
    }

    private void getMenuAnak() {
        getIdAnak(idAnak -> {
            for (int i = 0; i < idAnak.size(); i++) {
                addListAnak(idAnak.get(i));
            }
        });
    }

    private void getIdAnak(AnakListCallback anakListCallback) {
        FirebaseDatabase.getInstance().getReference("DataParents").child("Parents")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            String idAnak = dataSnapshot.getValue().toString();
                            idAnak = idAnak.substring(1, idAnak.length()-1);
                            String[] splitted = idAnak.split(",");
                            splitted_end = Arrays.copyOf(splitted, splitted.length);
                        }
                        if (splitted_end != null) {
                            for (int i = 0; i < splitted_end.length; i++) {
                                splitted_end[i] = splitted_end[i].substring(splitted_end[i].lastIndexOf("{")+1,
                                        splitted_end[i].lastIndexOf("="));
                                idAnak.add(splitted_end[i]);
                            }
                        }
                        anakListCallback.onCallback(idAnak);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void addListAnak(String id) {
        FirebaseDatabase.getInstance().getReference("DataBabies").child("Babies")
                .child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Anak anak = new Anak();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    anak.setIdAnak(id);
                    anak.setNamaAnak(dataSnapshot.getValue(Anak.class).getNamaAnak());
                }
                setAnak(anak);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setAnak(Anak anak) {
        Log.d("IDANAK", "ID anak: "+idAnak.toString());
        namaAnak.add(anak.getNamaAnak());
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, namaAnak
        );
        arrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        menuAnak.setAdapter(arrayAdapter);

       menuAnak.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               setListImunisasi(idAnak.get(position));
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });


    }

    private void setListImunisasi(String id) {
        listImunisasi.add(new Imunisasi("0 - 7 Hari", "Imunisasi", (new String[]{"HBO"})));
        listImunisasi.add(new Imunisasi("1 Bulan", "Imunisasi", (new String[]{"BCG", "Polio 1"})));
        listImunisasi.add(new Imunisasi("2 Bulan", "Imunisasi", (new String[]{"DPT-HB-Hib 1", "Polio 2"})));
        listImunisasi.add(new Imunisasi("3 Bulan", "Imunisasi", (new String[]{"DPT-HB-Hib 2", "Polio 3"})));
        listImunisasi.add(new Imunisasi("4 Bulan", "Imunisasi", (new String[]{"DPT-HB-Hib 3", "Polio 4"})));
        listImunisasi.add(new Imunisasi("9 Bulan", "Imunisasi", (new String[]{"Campak"})));
        listImunisasi.add(new Imunisasi("18 Bulan", "Imunisasi", (new String[]{"DPT-HB-Hib lanjutan", "Campak lanjutan"})));

        RecyclerView rcView = (RecyclerView) findViewById(R.id.recyclerview_template);
        ImunisasiViewAdapter imunisasiViewAdapter = new ImunisasiViewAdapter(this, listImunisasi,
                id);

        rcView.setLayoutManager(new GridLayoutManager(this, 1));
        rcView.setAdapter(imunisasiViewAdapter);
    }
}
